-- CRIANDO UM PACKAGE SPECIFICATION

CREATE OR REPLACE PACKAGE PCK_EMPREGADOS IS
 -- componentes publicas (variaveis, excessoes, procedures, funções)
 -- colocado apenas as assinaturas
 -- variaveis sao globais aqui
    GMINSALARY EMPLOYEES.SALARY%TYPE;
    PROCEDURE PRC_INSERE_EMPREGADO(
        PFIRST_NAME IN VARCHAR2,
        PLAST_NAME IN VARCHAR2,
        PEMAIL IN VARCHAR2,
        PPHONE_NUMBER IN VARCHAR2,
        PHIRE_DATE IN DATE,
        PJOB_ID IN VARCHAR2,
        PSALARY IN NUMBER,
        PCOMMICION_PCT IN NUMBER,
        PMANAGER_ID IN NUMBER,
        PDEPARTMENT_ID IN NUMBER
    );
    PROCEDURE PRC_AUMENTA_SALARIO_EMPREGADO(
        PEMPLOYEE_ID IN NUMBER,
        PPERCENTUAL IN NUMBER
    );
    FUNCTION FNC_CONSULTA_SALARIO_EMPREGADO(
        PEMPLOYEE_ID IN NUMBER
    ) RETURN NUMBER;
END PCK_EMPREGADOS;
 
 
 
 -- CRIANDO UM PACKAGE BODY
CREATE OR REPLACE PACKAGE BODY PCK_EMPREGADOS IS
 -- objetos criados aqui serão privados (se nao forem referenciados no package specification)
    PROCEDURE PRC_INSERE_EMPREGADO(
        PFIRST_NAME IN VARCHAR2,
        PLAST_NAME IN VARCHAR2,
        PEMAIL IN VARCHAR2,
        PPHONE_NUMBER IN VARCHAR2,
        PHIRE_DATE IN DATE,
        PJOB_ID IN VARCHAR2,
        PSALARY IN NUMBER,
        PCOMMICION_PCT IN NUMBER,
        PMANAGER_ID IN NUMBER,
        PDEPARTMENT_ID IN NUMBER
    ) IS
 -- nenhuma variavel declarada
    BEGIN
        INSERT INTO EMPLOYEES (
            EMPLOYEE_ID,
            FIRST_NAME,
            LAST_NAME,
            EMAIL,
            PHONE_NUMBER,
            HIRE_DATE,
            JOB_ID,
            SALARY,
            COMMISSION_PCT,
            MANAGER_ID,
            DEPARTMENT_ID
        ) VALUES (
            EMPLOYEES_SEQ.NEXTVAL,
            PFIRST_NAME,
            PLAST_NAME,
            PEMAIL,
            PPHONE_NUMBER,
            PHIRE_DATE,
            PJOB_ID,
            PSALARY,
            PCOMMICION_PCT,
            PMANAGER_ID,
            PDEPARTMENT_ID
        );
    EXCEPTION
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Erro Oracle ' || SQLCODE || SQLERRM);
    END;

    -------------------------------------------------------------------------------------------------

    PROCEDURE PRC_AUMENTA_SALARIO_EMPREGADO(
        PEMPLOYEE_ID IN NUMBER,
        PPERCENTUAL IN NUMBER
    ) IS
        -- sem variaveis
    BEGIN
        UPDATE EMPLOYEES
        SET SALARY = SALARY * (1 + ppercentual / 100)
        WHERE EMPLOYEE_ID = pemployee_id;
    EXCEPTION 
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Erro Oracle ' || SQLCODE || SQLERRM);
    END;

    -------------------------------------------------------------------------------------------------

    FUNCTION FNC_CONSULTA_SALARIO_EMPREGADO(
        PEMPLOYEE_ID IN NUMBER
    ) RETURN NUMBER
    IS
        vSalary EMPLOYEES.SALARY%TYPE;
    BEGIN
        SELECT SALARY INTO vSalary
        FROM EMPLOYEES
        WHERE EMPLOYEE_ID = pemployee_id;

        RETURN (vSalary);
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'Empregado inexistente');
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20001, 'Erro Oracle ' || SQLCODE || SQLERRM);
    END;

END PCK_EMPREGADOS;

-- EXCLUIR OBJETOS REDUNDANTES FORA DO PACKAGE;
DROP PROCEDURE PRC_AUMENTA_SALARIO_EMPREGADO;
DROP PROCEDURE PRC_INSERE_EMPREGADO;
DROP FUNCTION FNC_CONSULTA_SALARIO_EMPREGADO;