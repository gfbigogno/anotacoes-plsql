-- GERENCIANDO PROCEDURES E FUNÇÕES --
-- PODEMOS CONSULTAR POR MEIO DO DICIONARIO DE DADOS DA ORACLE

DESC USER_OBJECTS;

SELECT * FROM USER_OBJECTS;

SELECT object_name, object_type, last_ddl_time, 
timestamp, status
FROM USER_OBJECTS
WHERE object_type IN ('PROCEDURE', 'FUNCTION');

-- Objetos do usuários e aqueles que o usuário possui privilegios
SELECT object_name, object_type, last_ddl_time, 
timestamp, status
FROM ALL_OBJECTS
WHERE object_type IN ('PROCEDURE', 'FUNCTION');

-- Objetos inválidos do usuário atual
SELECT object_name, object_type, last_ddl_time, 
timestamp, status
FROM USER_OBJECTS
WHERE status = 'INVALID';

-- CODIGO FONTE DAS PROCEDURES E FUNCTIONS
DESC USER_SOURCE;

SELECT line, TEXT
FROM USER_SOURCE
WHERE name = 'PRC_INSERE_EMPREGADO' AND TYPE = 'PROCEDURE'
ORDER BY line;

SELECT line, TEXT
FROM USER_SOURCE
WHERE name = 'FNC_CONSULTA_SALARIO_EMPREGADO' AND TYPE = 'FUNCTION'
ORDER BY line;

-- PARA CONSULTAR LISTA DE PARAMETROS DE PROCEDURES E FUNÇÕES
DESC PRC_INSERE_EMPREGADO;
DESC LOCATION_DETAILS;

-- CONSULTANDO ERROS DE COMPILAÇÃO (sql plus)
SHOW ERRORS PROCEDURE SECURE_DML;

SHOW ERRORS FUNCTION FNC_;

SHOW ERRORS PACKAGE PACK_;

-- podemos verificar tambem por
COLUMN position FORMAT a4
COLUMN text FORMAT a60
SELECT line||'/'||position position, text
FROM   user_errors
WHERE  name = 'FNC_CONSULTA_SALARIO_EMPREGADO'
ORDER BY line;
