-- DEBUGANDO FUNCTIONS
-- Connect as SYSDBA
connect sys/<oracle>@<XE> as sysdba

-- Grant debug privileges to user
GRANT DEBUG CONNECT SESSION TO hr;
GRANT DEBUG ANY PROCEDURE TO hr;

-- Set up ACL to allow the database to connect back to Visual Studio Code
-- IP address and ports are those used for debugging on local VS Code machine
-- This needs to be done once for the machine.
BEGIN
  DBMS_NETWORK_ACL_ADMIN.APPEND_HOST_ACE(
  HOST => '127.0.0.1',
  LOWER_PORT => null,
  UPPER_PORT => null,
  ACE => XS$ACE_TYPE(PRIVILEGE_LIST => XS$NAME_LIST('jdwp'),
    PRINCIPAL_NAME =>'hr',
    PRINCIPAL_TYPE => XS_ACL.PTYPE_DB));
END;
