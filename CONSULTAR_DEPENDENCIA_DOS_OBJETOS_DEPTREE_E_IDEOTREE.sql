-- consultar dependencia dos objetos DEPTREE E IDEPTREE
-- executar o script utldtree.sql
-- C:\app\gusta\product\18.0.0\dbhomeXE
@C:\app\gusta\product\18.0.0\dbhomeXE\rdbms\admin\utldtree.sql

EXEC DEPTREE_FILL('TABLE', 'HR', 'EMPLOYEES');

DESC DEPTREE;

-- CASO EU MODIFIQUE EMPLOYEES, OS OBJETOS LISTADOS DEVEM SER REVISTOS PARA O DEVIDO FUNCIONAMENTO
SELECT *
FROM DEPTREE
ORDER BY seq#;

-- OUTRA MANEIRA (DE MANEIRA IDENTADA)

DESC IDEPTREE;
SELECT *
FROM ideptree;