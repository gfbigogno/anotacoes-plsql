-- TRIGGERS A NÍVEL DE COMANDO

CREATE OR REPLACE TRIGGER nome_trigger
BEFORE INSERT OR DELETE ON COUNTRIES
DECLARE

BEGIN

IF INSERTNG THEN
    -- faz algo caso algo foi inserido
END IF;

IF DELETING THEN
    -- faz algo caso algo foi deletado
END IF;

IF UPDATING THEN
    -- faz algo caso algo foi atualizado
END IF;

EXCEPTION

END;


CREATE OR REPLACE TRIGGER B_I_EMPLOYEES_S_TRG
BEFORE INSERT
ON EMPLOYEES
BEGIN
    IF  (TO_CHAR(SYSDATE, 'DAY') IN ('SABADO', 'DOMINGO') OR 
        TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) NOT BETWEEN 8 AND 18) 
    THEN

    RAISE_APPLICATION_ERROR(-20001, 'O cadastramento de empregados só é permitido em dias
    de semana dentro do horário comercial');

    END IF;
END;

-- PARA DELETAR UMA TRIGGER
DROP TRIGGER B_I_EMPLOYEES_S_TRG;
DROP TRIGGER B_I_EMPLOYEES_R_TRG;


BEGIN 
    PCK_EMPREGADOS.PRC_INSERE_EMPREGADO(PFIRST_NAME  => 'George',
                                        PLAST_NAME  => 'Harrison',
                                        PEMAIL  => 'GHARRISON',
                                        PPHONE_NUMBER  => '515.256.5690',
                                        PHIRE_DATE  => SYSDATE,
                                        PJOB_ID  => 'IT_PROG',
                                        PSALARY  => 25000,
                                        PCOMMICION_PCT  => NULL,
                                        PMANAGER_ID  => 103,
                                        PDEPARTMENT_ID  => 60);
    
    COMMIT;
END;

SELECT * FROM EMPLOYEES e where first_name = 'George';

DELETE EMPLOYEES where FIRST_NAME = 'George';

-- MESMA TRIGGER COMBINANDO VÁRIOS EVENTOS

CREATE OR REPLACE TRIGGER B_I_EMPLOYEES_S_TRG
BEFORE INSERT OR UPDATE OR DELETE
ON EMPLOYEES
BEGIN
    IF  (TO_CHAR(SYSDATE, 'DAY') IN ('SABADO', 'DOMINGO') OR 
        TO_NUMBER(TO_CHAR(SYSDATE, 'HH24')) NOT BETWEEN 16 AND 18) 
    THEN
        CASE
            WHEN INSERTING THEN

                RAISE_APPLICATION_ERROR(-20001, 'O cadastramento de empregados só é permitido em dias
        de semana dentro do horário comercial');

            WHEN DELETING THEN

                RAISE_APPLICATION_ERROR(-20002, 'A deleção de empregados só é permitido em dias
        de semana dentro do horário comercial');

            ELSE

                RAISE_APPLICATION_ERROR(-20003, 'A atualização de empregados só é permitido em dias
        de semana dentro do horário comercial');

        END CASE;      
    END IF;
END;


SELECT * FROM EMPLOYEES e where EMPLOYEE_ID = 208;

BEGIN 
    PCK_EMPREGADOS.PRC_AUMENTA_SALARIO_EMPREGADO(PEMPLOYEE_ID  => 208,
                                                 PPERCENTUAL  => 15);
    COMMIT;
END;