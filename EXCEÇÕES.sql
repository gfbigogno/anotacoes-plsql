-- EXCEÇÕES --

/*
COLLECTION_IS_NULL
CURSOR_ALREADY_OPEN
DUP_VAL_ON_INDEX (tentar inserir valor duplicado em col unica)
LOGIN_DENIED
VALUE_ERROR (erro de aritmética)

 - interrupção do sistema -
 RAISE_APPLICATION_ERROR(cod, msg) - a mensagem pode ser capturada pelo sistema implementado

SQLCODE - retorna codigo sql
SQLERRM - retorna mensagem do erro que disparou a excessao
*/

ACCEPT pEmp_id PROMPT 'Digite o id do funcionario: ';

DECLARE
    vFirst_name EMPLOYEES.FIRST_NAME%TYPE;
    vLast_name EMPLOYEES.LAST_NAME%TYPE;
    vEmployee_id EMPLOYEES.EMPLOYEE_ID%TYPE := &pEmp_id;
BEGIN
    SELECT FIRST_NAME, LAST_NAME
    INTO vFirst_name, vLast_name
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID = vEmployee_id;

    DBMS_OUTPUT.PUT_LINE('Empregado: ' || vFirst_name || ' ' || vLast_name);

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'Empregado não encontrado, id = ' ||
            TO_CHAR(vEmployee_id));
    
    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20002, 'Erro oracle - ' ||
            SQLCODE || SQLERRM);
END;

--------------------------------------------
-- EXECUÇÕES DEFINIDAS PELO DESENVOLVEDOR --
--------------------------------------------

ACCEPT pEmp_id PROMPT 'Digite o id do funcionario: ';

DECLARE
    vFirst_name EMPLOYEES.FIRST_NAME%TYPE;
    vLast_name EMPLOYEES.LAST_NAME%TYPE;
    vEmployee_id EMPLOYEES.EMPLOYEE_ID%TYPE := &pEmp_id;
    vJob_id EMPLOYEES.JOB_ID%TYPE;
    ePresident EXCEPTION;
BEGIN
    SELECT FIRST_NAME, LAST_NAME, JOB_ID
    INTO vFirst_name, vLast_name, vJob_id
    FROM EMPLOYEES
    WHERE EMPLOYEE_ID = vEmployee_id;

    IF vJob_id = 'AD_PRES' THEN
        RAISE ePresident;       -- nao segue o fluxo do programa (o pessoal nao usa muito)
    END IF;

    DBMS_OUTPUT.PUT_LINE('Empregado: ' || vFirst_name || ' ' || vLast_name);

EXCEPTION

    WHEN NO_DATA_FOUND THEN
        RAISE_APPLICATION_ERROR(-20001, 'Empregado não encontrado, id = ' ||
            TO_CHAR(vEmployee_id));

    WHEN ePresident THEN
        UPDATE EMPLOYEES
        SET   salary = salary*1.5
        WHERE EMPLOYEE_ID = vEmployee_id;
        COMMIT;

    WHEN OTHERS THEN
        RAISE_APPLICATION_ERROR(-20002, 'Erro oracle - ' ||
            SQLCODE || SQLERRM);
END;

--SELECT * FROM EMPLOYEES WHERE JOB_ID = 'AD_PRES';


-- PRAGMA EXCEPTION_INIT
DECLARE
   vemployee_id    employees.employee_id%TYPE := 300;
   vfirst_name     employees.first_name%TYPE := 'Robert';
   vlast_name      employees.last_name%TYPE := 'Ford';
   vjob_id         employees.job_id%TYPE := 'XX_YYYY';
   vphone_number   employees.phone_number%TYPE := '650.511.9844';
   vemail          employees.email%TYPE := 'RFORD';
   efk_inexistente EXCEPTION;
   PRAGMA EXCEPTION_INIT(efk_inexistente, -2291);

BEGIN
   INSERT INTO employees (employee_id, first_name, last_name, phone_number, email, hire_date,job_id)
   VALUES (vemployee_id, vfirst_name, vlast_name, vphone_number, vemail, sysdate, vjob_id);
EXCEPTION
   WHEN  efk_inexistente 
   THEN
         RAISE_APPLICATION_ERROR(-20003, 'Job inexistente!');
   WHEN OTHERS 
   THEN
         RAISE_APPLICATION_ERROR(-20002, 'Erro Oracle ' || SQLCODE || SQLERRM);
END;

