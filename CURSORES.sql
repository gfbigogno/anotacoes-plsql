<<exemplo_de_cursor_chatgpt>>
DECLARE
    TYPE emp_tipo_record IS RECORD
    (
        first_name EMPLOYEES.FIRST_NAME%TYPE,
        last_name EMPLOYEES.LAST_NAME%TYPE
    );

    emp_record emp_tipo_record;

    CURSOR c_nome_funcionarios IS
        SELECT FIRST_NAME, LAST_NAME
        FROM EMPLOYEES
        WHERE DEPARTMENT_ID = 60;
BEGIN

    -- Abrindo cursor
    OPEN c_nome_funcionarios;

    -- iterar cursor
    LOOP

        -- busca proximo registro
        FETCH c_nome_funcionarios
        INTO emp_record.first_name, emp_record.last_name;

        DBMS_OUTPUT.PUT_LINE(
            emp_record.first_name || ' ' || emp_record.last_name
        );

        EXIT WHEN c_nome_funcionarios%NOTFOUND; 
    END LOOP;

    -- fechar cursor
    CLOSE c_nome_funcionarios;
END;



------ UTILIZAÇÃO DE VARRAY --------
------ DEVE SER ESPECIFICADO O TAMANHO MÁXIMO
------ NA PRÁTICA, O MAIS UTILIZADO É O ASSOCIATIVE ARRAY
<<exemplo_varray>>
DECLARE
    TYPE emp_tipo_varray
    iS VARRAY (20) OF EMPLOYEES.FIRST_NAME%TYPE;

    emp_varray emp_tipo_varray := emp_tipo_varray();
BEGIN

    -- da mesma forma da nested table
    FOR i IN 1..20 LOOP
        emp_varray.extend;
        emp_varray(i) := i;
    END LOOP;

    FOR i IN 1..20 LOOP
        DBMS_OUTPUT.PUT_LINE('valor: '|| emp_varray(i));
    END LOOP;
END;

<<exemplo_varray_com_bulk_collect>>
DECLARE
    TYPE emp_tipo_varray
    IS VARRAY (200) OF EMPLOYEES%rowtype;

    emp_varray emp_tipo_varray := emp_tipo_varray();
BEGIN

    SELECT *
    BULK COLLECT
    INTO emp_varray
    FROM EMPLOYEES;

    FOR i IN emp_varray.first..emp_varray.last LOOP
        DBMS_OUTPUT.PUT_LINE(emp_varray(i).first_name ||
         ' ' || emp_varray(i).last_name); 
    END LOOP;
END;

/* PARÂMETROS PARA COLLECTIONS
    EXISTS(n)
    COUNT
    LAST
    FIRST
    LIMIT --> varray
    PRIOR(n) [retorna indice anterior]
    NEXT(n) [retorna indice posterior]
    EXTEND(n) --> nested table e varray
    DELETE(n) [remove elemento n de uma collection]
*/