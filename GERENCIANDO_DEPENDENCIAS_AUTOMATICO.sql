-- GERENCIANDO DEPENDENCIAS AUTOMATICO DO ORACLE

/*      !!!
FAÇA OS DEVIDOS AJUSTES E RE-COMPILE TODOS OS OBJETOS QUE POSSUEM
DEPENDÊNCIA DERETA OU INDIRETA DO OBJETO ALTERADO
*/

DESC USER_DEPENDENCIES;

-- LISTAR DEPENDENCIAS DIRETAS
SELECT *
FROM USER_DEPENDENCIES
WHERE REFERENCED_NAME = 'EMPLOYEES' AND
REFERENCED_TYPE = 'TABLE';

/*
DEPOIS DE MODIFICAR A TABELA EMPLOYEES, VERIFICAR TODAS AS 
DEPENDENCIAS LISTADAS ANTERIORMENTE, A FIM DE VERIFICAR SE
OS IMPACTARÁ, AJUSTAR E TESTAR.
*/

-- LISTAR DEPENDENCIAS INDIRETAS (MUITO IMPORTANTE)
SELECT *
FROM USER_DEPENDENCIES
START WITH REFERENCED_NAME = 'EMPLOYEES' AND
REFERENCED_TYPE = 'TABLE'
CONNECT BY PRIOR name = REFERENCED_NAME AND
TYPE = REFERENCED_TYPE;

-- Consultando objetos Inválidos do schema do seu usuário 
DESC USER_OBJECTS

SELECT object_name, object_type, last_ddl_time, timestamp, status
FROM   user_objects
WHERE  status = 'INVALID';