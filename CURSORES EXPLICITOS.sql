-- CURSORES EXPLICITOS --
-- PARA CONSULTAS QUE RETORNAM MAIS DE UMA LINHA
-- CONTROLE TOTAL SOBRE O CURSOR
-- PROCESSAR INDIVIDUALMENTE CADA LINHA RETORNADA

/* ATRIBUTOS DE CURSOR EXPLÍCITO

%ISOPEN
%NOTFOUND
%FOUND
%ROWCOUNT [numero de linhas recuperadas pelo fetch até o momento]

*/

<<cursor_para_mostrar_nomes_com_a>>
DECLARE
    CURSOR emp_cursor IS
    SELECT *
    FROM EMPLOYEES;

    emp_record EMPLOYEES%rowtype;
BEGIN

    OPEN emp_cursor;

    LOOP

        FETCH emp_cursor
        INTO emp_record;

        IF emp_record.first_name LIKE 'A%' THEN

            DBMS_OUTPUT.PUT_LINE('Nome: '
            || emp_record.first_name || emp_record.last_name);
            DBMS_OUTPUT.PUT_LINE('Email: ' || emp_record.Email);
        
        END IF;

        EXIT WHEN emp_cursor%notfound;
    END LOOP;

END;

-- EXEMPLO COM WHILE
<<cursor_para_mostrar_nomes_com_a>>
DECLARE
    CURSOR emp_cursor IS
    SELECT *
    FROM EMPLOYEES;

    emp_record EMPLOYEES%rowtype;
BEGIN

    OPEN emp_cursor;

    FETCH emp_cursor
    INTO emp_record;

    WHILE emp_cursor%found LOOP

        IF emp_record.first_name LIKE 'A%' THEN

            DBMS_OUTPUT.PUT_LINE('Nome: '
            || emp_record.first_name || emp_record.last_name);
            DBMS_OUTPUT.PUT_LINE('Email: ' || emp_record.Email);
        
        END IF;

        FETCH emp_cursor
        INTO emp_record;

    END LOOP;

END;

-- CURSOR FOR LOOP (MAIS UTILIZADO) [UTILIZADO NA PRÁTICA]
-- DENTRO DO LOOP ESTÁ IMPLICITO A ABERTURA, O FETCH E O FECHAMENTO DO CURSOR
-- A VARIAVEL RECORD É DECLARADA AUTOMATICAMENTE
DECLARE
    CURSOR emp_cursor IS
    SELECT *
    FROM EMPLOYEES;
BEGIN

        -- VARIAVEL_REC IN NOME_CURSOR
    FOR emp_record IN emp_cursor LOOP -- OU FOR emp_record IN (SELECT * FROM EMPLOYEES)
        DBMS_OUTPUT.PUT_LINE('Email: ' || emp_record.email);
    END LOOP;

END;

-- CURSOR EXPLICITO COM PARÂMETROS
-- PARAMETROS PASSADOS NO OPEN
DECLARE
    CURSOR emp_cursor(pdepartment_id EMPLOYEES.DEPARTMENT_ID%TYPE) IS
    SELECT LAST_NAME, EMAIL
    FROM EMPLOYEES
    WHERE pdepartment_id = DEPARTMENT_ID;
BEGIN
    FOR emp_rec IN emp_cursor(30) LOOP
        DBMS_OUTPUT.PUT_LINE('Email: ' || emp_rec.email);
    END LOOP;
END;

-- CURSOR EXPLICITO COM SELECT FOR UPDATE

