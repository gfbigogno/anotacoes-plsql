-- DML TRIGGERS
/*
    Isso significa que a trigger pode modificar ou validar os dados
    antes que a ação seja realizada. Já uma trigger "after" é executada
    após a ação ter sido concluída, permitindo que a trigger acesse
    e manipule os dados resultantes da ação.
*/

CREATE OR REPLACE TRIGGER 