SET SERVEROUTPUT ON;

-- Método pouco utilizado
<< exemplo_de_nested_table_sem_consulta >>
DECLARE
	TYPE product_list IS
        TABLE OF VARCHAR2(20);

product_array product_list := product_list();
BEGIN
	FOR i IN 1..10 LOOP
		product_array.extend;
		product_array(i) := i || ' pos ';
	END LOOP;

	FOR i IN product_array.first..product_array.last LOOP
		dbms_output.put_line('Valor anterior - ' || product_array.last);
		product_array.extend;
		product_array(product_array.last) := product_array(i);
		dbms_output.put_line(i || ' - ' || product_array.last);
		-- dbms_output.put_line(product_array(i));
	END LOOP;

	FOR i IN product_array.first..product_array.last LOOP
		dbms_output.put_line('Valor novo - ' || i);
	END LOOP;
END;

<< utilizando_nested_table_e_record_variables >> DECLARE
    TYPE emp_record IS RECORD (
        first_name employees.first_name%TYPE,
        last_name  employees.last_name%TYPE
    );
    TYPE emp_list IS
        TABLE OF emp_record;
    emp_array emp_list := emp_list();
BEGIN
	
    SELECT
        first_name,
        last_name
    BULK COLLECT
    INTO emp_array
    FROM
        employees
    WHERE
        job_id = 'AD_VP';

    FOR i IN 1..emp_array.count LOOP
        dbms_output.put_line(emp_array(i).first_name);
    END LOOP;

END;


<<exemplo_tipo_array_associativo_com_bulkcollect>>
DECLARE
    TYPE location_record IS RECORD (
        street_address locations.street_address%TYPE,
        city           locations.city%TYPE,
        country_name   countries.country_name%TYPE
    );
    TYPE location_list IS
        TABLE OF location_record INDEX BY BINARY_INTEGER;
    location_array location_list;
BEGIN
    SELECT
        l.street_address,
        l.city,
        c.country_name
    BULK COLLECT
    INTO location_array
    FROM
        locations l
        JOIN countries c ON l.country_id = c.country_id;

    FOR i IN 1..location_array.count LOOP
        dbms_output.put_line(location_array(i).street_address
                             || ' || '
                             || location_array(i).city
                             || ' || '
                             || location_array(i).country_name);
    END LOOP;

END;

CREATE OR REPLACE FUNCTION location_details (pRegion_id IN countries.region_id%type)
RETURN VARCHAR2
IS
    TYPE location_record IS RECORD
    (
        street_address locations.street_address%type,
        city locations.city%type,
        country_name countries.country_name%type
    );
    TYPE location_list IS TABLE OF location_record INDEX BY BINARY_INTEGER;
    location_array location_list;
    
    vResultado VARCHAR2(1000) := '';
BEGIN
    SELECT
        l.street_address,
        l.city,
        c.country_name
    BULK COLLECT
    INTO location_array
    FROM
        locations l
        JOIN countries c ON l.country_id = c.country_id
    WHERE
        region_id = pRegion_id;

    FOR i IN 1..location_array.COUNT LOOP
        vResultado := vResultado || location_array(i).street_address
                             || ' | '
                             || location_array(i).city
                             || ' | '
                             || location_array(i).country_name
                             || CHR(10);
    END LOOP;
    
    RETURN vResultado;
END;


<<EXEMPLO_FUNCAO_RETORNO_RECORD>>
DECLARE
	TYPE employee_record IS RECORD (
    id NUMBER,
	first_name VARCHAR2(30),
	last_name VARCHAR2(30),
	salary NUMBER
  );
 
emp_record employee_record;

FUNCTION get_employee (emp_id NUMBER) RETURN employee_record IS
    emp_info employee_record;
BEGIN
	SELECT
	EMPLOYEE_ID,
	first_name,
	last_name,
	salary
    INTO
	emp_info
FROM
	employees
WHERE
	EMPLOYEE_ID = emp_id;

RETURN emp_info;
END;

BEGIN
  	emp_record := get_employee(123);

	DBMS_OUTPUT.PUT_LINE('ID: ' || emp_record.id);
	
	DBMS_OUTPUT.PUT_LINE('First Name: ' || emp_record.first_name);
	
	DBMS_OUTPUT.PUT_LINE('Last Name: ' || emp_record.last_name);
	
	DBMS_OUTPUT.PUT_LINE('Salary: ' || emp_record.salary);
END;


BEGIN
    dbms_output.put_line(location_details(2));
END;




