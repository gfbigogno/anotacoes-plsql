-- REFENCIANDO COMPONENTES DE UMA PACKAGE
DECLARE
    vSalary EMPLOYEES.SALARY%TYPE;
BEGIN

    vSalary := PCK_EMPREGADOS.FNC_CONSULTA_SALARIO_EMPREGADO(PEMPLOYEE_ID  => 100);
    DBMS_OUTPUT.PUT_LINE('Salario: ' || vSalary);

END;

BEGIN
    PCK_EMPREGADOS.PRC_INSERE_EMPREGADO(PFIRST_NAME  => 'Bob',
                                        PLAST_NAME  => 'Dylan',
                                        PEMAIL  => 'BDYLAN',
                                        PPHONE_NUMBER  => '515.258.4861',
                                        PHIRE_DATE  => SYSDATE,
                                        PJOB_ID  => 'IT_PROG',
                                        PSALARY  => 20000,
                                        PCOMMICION_PCT  => NULL,
                                        PMANAGER_ID  => 103,
                                        PDEPARTMENT_ID  => 60);
END;

SELECT * FROM EMPLOYEES WHERE FIRST_NAME LIKE 'Bob%';
SELECT * FROM EMPLOYEES WHERE EMPLOYEE_ID = 208;